
const http = require('http')
const {URL}= require('url')
const {EventEmitter}=require('events')
const zlib = require('zlib')
const sse = new EventEmitter
http.createServer(function (req, res) {
	try {
		console.log(req.headers)
		const u = new URL(req.url, 'http://' + req.headers.host)
		if (u.pathname === '/sse' && req.headers['accept'].includes('text/event-stream')) {
			const h = {'content-type': 'text/event-stream', 'connection': 'keep-alive'}
			const w = (...args) => Reflect.apply(w.write || w.self.write, w.self, args)
			if ('accept-encoding' in req.headers) {
				if (req.headers['accept-encoding'].includes('deflate')) {
					let z = zlib.createDeflate()
					h['content-encoding'] = 'deflate'
					w.self = z
					w.write = (data, fl) => z.write(data, () => z.flush(fl))
					z.pipe(res)
				} else if (req.headers['accept-encoding'].includes('gzip')) {
					let z = zlib.createGzip()
					h['content-encoding'] = 'gzip'
					w.self = z
					w.write = (data, fl) => z.write(data, () => z.flush(fl))
					z.pipe(res)
				}
			} else w.self = res
			res.writeHead(200, h)

			w('retry: 100000\ndata: null\n\n')
			sse.on('data', w)
			res.once('close', () => sse.off('data', w))
		} else {
			res.writeHead(200, {'content-type':'text/html', 'connection': 'close'})
			res.end('Ohai')
		}
	} catch (e) {
		res.writeHead(500, {'content-type': 'text/error'})
		res.end(e.stack)
	}
}).listen(8000, function (){console.log(this, 'listening')})
const flush = () => console.log('flush')

setTimeout(function ping() {
	sse.emit('data', (Math.random() > 0.5 ? 'id: ' + Date.now() + '\n' : '') + 'event: ping\ndata: {"date":"' + new Date().toISOString() + '"}\n\n', flush)
	setTimeout(ping, Math.random() * 5e3)
}, Math.random() * 1e3)
