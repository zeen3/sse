# SSE - Server Sent Events

Old tech that uses long-polling; and a more workable alternative to websockets for chat and the like.

Note: The intent is to have workable SSE more easily. On a h2 interface there is no extra connection; so there is no requirement for another one. It can be sent as a push stream as well; allowing for easy initalisation and the like.

- `fetch-sse` in both `dist` and `src` are for a fetch-based polyfill to the original sse.
- `multi-sse` in both `dist` and `src` is for a subscription-based event reader; and has no faster happening.
- `sse.js` is something I put together in 5 minutes. It just sends `ping` events randomly. Can be useful for latency testing.

Basically just uh yeah. Stuff that doesn't need to happen but makes it easier anyway.

Though you don't get the neat tabular format from the `fetch` versions that you do from the `EventSource`; there can be more happening.
