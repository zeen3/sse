export interface Dispatchable {
    [event: string]: string;
}
export interface Ongoing {
    rqi: RequestInit;
    res: Response;
    txd: TextDecoder;
    body: ReadableStreamReader;
    chunks: Uint8ClampedArray[];
    current: {
        [event: string]: string[];
    };
    channels: string[];
    lastEventId: string;
}
export interface EventListenerSet {
    [listener: string]: (event: Event) => void;
}
export interface EventTargetListeners {
    target: EventTarget;
    listeners: EventListenerSet;
    options: object | undefined;
}
export declare class FetchMultiSSE extends EventTarget {
    private ongoing;
    private channels;
    constructor();
    getChannel(chan: string): EventTarget;
    deleteChannel(chan: string): void;
    addEventListeners(target: EventTarget, listeners: EventListenerSet, options: object | undefined): EventTargetListeners;
    removeEventListeners(l: EventTargetListeners): void;
    unlisten(url: string): void;
    listen(url: string, rqinit: RequestInit, channel?: string | string[], reset?: boolean): Promise<Ongoing>;
    close(url: string): boolean;
    private iterate;
    private dispatch;
}
export default FetchMultiSSE;
//# sourceMappingURL=multi-sse.d.ts.map