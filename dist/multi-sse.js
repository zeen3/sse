export class FetchMultiSSE extends EventTarget {
    constructor() {
        super();
        this.ongoing = new Map;
        this.channels = { 'default': new EventTarget };
    }
    getChannel(chan) {
        if (chan in this.channels)
            return this.channels[chan];
        else
            return (this.channels[chan] = new EventTarget);
    }
    deleteChannel(chan) {
        delete this.channels[chan];
    }
    addEventListeners(target, listeners, options) {
        for (const listener in listeners)
            if (listeners.hasOwnProperty(listener))
                target.addEventListener(listener, listeners[listener], options);
        return { target, listeners, options };
    }
    removeEventListeners(l) {
        for (const listener in l.listeners)
            if (l.listeners.hasOwnProperty(listener))
                l.target.removeEventListener(listener, l.listeners[listener], l.options);
    }
    unlisten(url) {
        const sse = this.ongoing.get(url);
        if (sse) {
            sse.body.cancel();
            this.ongoing.delete(url);
        }
    }
    listen(url, rqinit, channel = 'default', reset = false) {
        let rqi = 'headers' in rqinit
            ? Object.assign({}, rqinit, { headers: Object.assign({}, rqinit.headers, { accept: 'text/event-stream' }) }) : Object.assign({}, rqinit, { headers: { accept: 'text/event-stream' } });
        if ('mode' in rqi && rqi.mode === 'no-cors')
            return Promise.reject(new SyntaxError(`Server Event Source cannot be a no-cors Request.`));
        return fetch(url, rqi).then((res) => {
            if (res.status !== 200)
                throw new RangeError(`Server Event Source ${res.url || url} returned a non-200 response code.`);
            const content_type = res.headers.get('content-type');
            if (!content_type || !content_type.startsWith('text/event-stream'))
                throw new TypeError(`Server Event Source ${res.url || url} is not of type 'text/event-stream'.`);
            if (res.body === null)
                throw new TypeError(`Server Event Source ${res.url || url} does not have a readable body.`);
            let go = {
                rqi: rqi,
                res: res,
                txd: new TextDecoder,
                body: res.body.getReader(),
                chunks: [],
                current: {},
                channels: Array.isArray(channel)
                    ? channel
                    : [channel],
                lastEventId: ''
            };
            if (reset !== this.ongoing.has(res.url))
                throw new SyntaxError(`Server Event Source cannot be reset if it's not existing nor set if it's resetting.`);
            else
                this.ongoing.set(res.url, go);
            go.body.read().then(it => this.iterate(go, it));
            return go;
        });
    }
    close(url) {
        const go = this.ongoing.get(url);
        if (go) {
            go.body.cancel();
            this.ongoing.delete(url);
            return true;
        }
        else
            return false;
    }
    iterate(self, it) {
        if (it.done) {
            this.listen(self.res.url, self.rqi, self.channels, true);
            return Promise.reject('retrying');
        }
        let ch = it.value;
        for (let i = 0; i < ch.length; i++)
            if (ch[i] === 10) {
                self.chunks.push(new Uint8ClampedArray(ch.buffer, ch.byteOffset, i));
                const tx = self.chunks.reduce((s, chk, i) => s + self.txd.decode(chk, { stream: i !== self.chunks.length - 1 }), '');
                self.chunks.length = 0;
                const col = tx.indexOf(': ');
                const key = tx.slice(0, col);
                const val = tx.slice(col + 2);
                if (key in self.current)
                    self.current[key].push(val);
                else
                    self.current[key] = [val];
                if (i + 1 in ch && ch[i + 1] === 10) {
                    this.dispatch(self);
                    ch = new Uint8ClampedArray(ch.buffer, ch.byteOffset + i + 2, ch.length - i - 2);
                }
                else {
                    ch = new Uint8ClampedArray(ch.buffer, ch.byteOffset + i + 1, ch.length - i - 1);
                }
                i = -1;
            }
        if (ch.length > 0)
            self.chunks.push(ch);
        return self.body.read().then(it => this.iterate(self, it));
    }
    dispatch(self) {
        const dis = {};
        for (const key in self.current)
            if (self.current.hasOwnProperty(key))
                dis[key] = self.current[key].join('\n');
        self.current = {};
        if ('id' in dis)
            self.lastEventId = dis.id;
        const msgInit = {
            data: 'data' in dis ? dis.data : '',
            origin: self.res.url,
            lastEventId: self.lastEventId,
        };
        const msg = new MessageEvent('event' in dis ? dis.event : 'message', msgInit);
        this.dispatchEvent(msg);
        for (const chan of self.channels)
            this.getChannel(chan).dispatchEvent(msg);
    }
}
export default FetchMultiSSE;
//# sourceMappingURL=multi-sse.js.map