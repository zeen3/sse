"use strict";
class FetchEventSource extends EventTarget {
    constructor(url, rqinit = {}) {
        super();
        this.url = url;
        this.txd = new TextDecoder;
        this.chunks = [];
        this.currentEvent = {};
        this.lastEventId = '';
        this.rqinit = 'headers' in rqinit
            ? Object.assign({}, rqinit, { headers: Object.assign({}, rqinit.headers, { accept: 'text/event-stream' }) }) : Object.assign({}, rqinit, { headers: { accept: 'text/event-stream' } });
        fetch(String(url), this.rqinit).then(this.init).catch(this.err);
    }
    err(e) {
        console.error(e);
        throw e;
    }
    init(res) {
        this.res = res;
        if (!res.ok)
            throw new Error(`FetchEventSource ${res.url} returned an error`);
        if (res.headers.get('content-type') !== 'text/event-stream')
            throw new TypeError(`FetchEventSource ${res.url} is not of type 'text/event-stream'`);
        if (res.body === null)
            throw new Error(`FetchEventSource ${res.url} has no body`);
        const body = res.body.getReader();
        this.body = body;
        return body.read().then(it => this.iterate(body, it)).catch(e => {
            if (e === 'done') {
                console.error('Event streams should persist indefinitely; retrying...');
                return fetch(res.url, this.rqinit).then(this.init, this.err);
            }
            else
                throw e;
        });
    }
    iterate(body, it) {
        if (it.done)
            throw 'done';
        let chunk = it.value;
        for (let i = 0; i < chunk.length; i++)
            if (chunk[i] === 10) {
                this.chunks.push(new Uint8ClampedArray(chunk.buffer, chunk.byteOffset, i));
                let tx = this.chunks.reduce((s, chk, i) => s + this.txd.decode(chk, {
                    stream: i !== this.chunks.length - 1
                }), '');
                this.chunks.length = 0;
                let colon = tx.indexOf(': ');
                let key = tx.slice(0, colon);
                this.currentEvent[key] = key in this.currentEvent
                    ? [...this.currentEvent[key], tx.slice(colon + 2)]
                    : [tx.slice(colon + 2)];
                if (chunk[i] === chunk[i + 1]) {
                    this.dispatch();
                    chunk = new Uint8ClampedArray(chunk.buffer, chunk.byteOffset + i + 2, chunk.length - i - 2);
                }
                else
                    chunk = new Uint8ClampedArray(chunk.buffer, chunk.byteOffset + i + 1, chunk.length - i - 1);
                i = -1;
            }
        if (chunk.length > 0)
            this.chunks.push(chunk);
        return body.read().then(it => this.iterate(body, it));
    }
    dispatch() {
        const dispatchable = {};
        for (const key in this.currentEvent)
            if (this.currentEvent.hasOwnProperty(key))
                dispatchable[key] = this.currentEvent[key].join('\n');
        this.currentEvent = {};
        if ('id' in dispatchable)
            this.lastEventId = dispatchable.id;
        let msg = new MessageEvent('event' in dispatchable ? dispatchable.event : 'message', {
            data: 'data' in dispatchable ? dispatchable.data : '',
            lastEventId: this.lastEventId
        });
        this.dispatchEvent(msg);
    }
}
//# sourceMappingURL=fetch-sse.js.map