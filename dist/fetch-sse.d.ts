declare class FetchEventSource extends EventTarget {
    url: string | URL;
    lastEventId: string;
    res?: Response;
    body?: ReadableStreamReader;
    private rqinit;
    private txd;
    private currentEvent;
    private chunks;
    constructor(url: string | URL, rqinit?: RequestInit);
    private err;
    private init;
    private iterate;
    private dispatch;
}
//# sourceMappingURL=fetch-sse.d.ts.map